defmodule TodoList.Repo.Migrations.CreateTodoItems do
  use Ecto.Migration

  def change do
    create table(:todo_items) do
      add(:item, :string)
      add(:todo_id, references(:todos, on_delete: :delete_all))

      timestamps()
    end

    create(index(:todo_items, [:todo_id]))
  end
end
