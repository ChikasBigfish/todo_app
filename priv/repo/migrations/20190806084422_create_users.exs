defmodule TodoList.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :password, :string
      add :username, :string
      add :confirmed_at, :utc_datetime


      timestamps()
    end
  end
end
