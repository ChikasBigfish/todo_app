defmodule TodoListWeb.Router do
  use TodoListWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TodoListWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
    resources("/signin", SessionController, only: [:new, :create])
    resources("/signup", RegistrationController, only: [:new, :create])
    get("/sign_out", SessionController, :delete)

    resources "/todos", TodoController do
      resources("/items", ItemController, only: [:create])
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", TodoListWeb do
  #   pipe_through :api
  # end
end
