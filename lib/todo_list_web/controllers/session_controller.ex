defmodule TodoListWeb.SessionController do
  use TodoListWeb, :controller

  alias TodoList.Auth

  def new(conn, _params) do
    render(conn, "signin.html")
  end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    case Auth.sign_in(email, password) do
      {:ok, user} ->
        conn

        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "You have successfully signed in.")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Invalid email or password")
        |> render("signin.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Auth.sign_out()
    |> redirect(to: Routes.page_path(conn, :index))
  end

end
