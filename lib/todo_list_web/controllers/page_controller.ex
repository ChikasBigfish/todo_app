defmodule TodoListWeb.PageController do
  use TodoListWeb, :controller

  alias TodoList.Chat

  def index(conn, _params) do
    todo =Chat.list_todos()
    render(conn, "index.html", todo: todo)
  end
end
