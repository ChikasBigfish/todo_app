defmodule TodoListWeb.ItemController do
  use TodoListWeb, :controller

   alias TodoList.Repo
   alias TodoList.Item
   alias TodoList.Todo
   alias TodoList.Todo.Item
   alias TodoList.Item.Items

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"todo_id" => todo_id, "item" => item_params}) do
    todo = Items.get_todo!(todo_id)

    case Items.create_item(todo, item_params) do
      {:ok, _item} ->
        conn
        |> put_flash(:info, "Item created successfully")
        |> render(to: Routes.todo_path(conn, :show, todo))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Issue creating Item")

        render(conn, "new.html", changeset: changeset)
    end
  end
end
