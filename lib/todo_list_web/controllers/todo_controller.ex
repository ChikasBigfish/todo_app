defmodule TodoListWeb.TodoController do
  use TodoListWeb, :controller

  alias TodoList.Chat
  alias TodoList.Todo
  # alias TodoList.Repo
  alias TodoList.Todo.Item
  alias TodoList.Item.Items

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def new(conn, _params) do
    changeset = Chat.change_todo(%Todo{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"todo" => todo_params}) do
    case Chat.create(todo_params) do
      {:ok, todo} ->
        conn
        |> put_session(:current_todo_id, todo.id)
        |> put_flash(:info, "Todo created successfully")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end


  def show(conn, %{"id" => id}) do
    todo = Items.get_todo!(id)
    items_changeset = Items.change_items(%Item{})
    render(conn, "show.html", content: todo, items_changeset: items_changeset)
  end

  def edit(conn, %{"id" => id}) do
    todo = Chat.get_todo!(id)
    changeset = Chat.change_todo(todo)
    render(conn, "edit.html", todo: todo, changeset: changeset)
  end

  def update(conn, %{"id" => id, "todo" => todo_params}) do
    todo = Chat.get_todo!(id)

    case Chat.update_todo(todo, todo_params) do
      {:ok, todo} ->
        conn
        |> put_flash(:info, "Todo updated successfully")
        |> redirect(to: Routes.todo_path(conn, :show, todo))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", todo: todo, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    todo = Chat.get_todo!(id)
    {:ok, _todo} = Chat.delete_todo(todo)

    conn
    |> put_flash(:info, "Todo deleted successfully")
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
