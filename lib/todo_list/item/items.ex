defmodule TodoList.Item.Items do
  alias TodoList.Todo
  alias TodoList.Repo
  alias TodoList.Todo.Item

  def create_item(%Todo{} = todo, attrs \\ %{}) do
    todo
    |> Ecto.build_assoc(:items)
    |> Item.changeset(attrs)
    |> Repo.insert()
  end

  def get_todo!(id) do
    Todo
    |> Repo.get!(id)
    |> Repo.preload(:items)
  end

  def change_items(%Item{} = item) do
    Item.changeset(item, %{})
  end
end
