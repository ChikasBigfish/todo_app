defmodule TodoList.Todo do
  use Ecto.Schema
  import Ecto.Changeset

  alias TodoList.User
  alias TodoList.Todo.Item

  schema "todos" do
    field(:title, :string)
    belongs_to(:user, User)
    has_many(:items, Item)
    timestamps()
  end

  @doc false
  def changeset(todo, attrs) do
    todo
    |> cast(attrs, [:title, :user_id])
    |> validate_required([:title])
  end
end
