defmodule TodoList.Chat do

  alias TodoList.Repo
  alias TodoList.Todo

  def create(params) do
    Todo.changeset(%Todo{}, params)
    |> Repo.insert()
  end

  def list_todos do
    Repo.all(Todo)
  end

  def get_todo!(id), do: Repo.get!(Todo, id)

  def change_todo(%Todo{} = todo) do
    Todo.changeset(todo, %{})
  end

  def update_todo(%Todo{} = todo, attrs) do
    todo
    |> Todo.changeset(attrs)
    |> Repo.update()
  end

  def delete_todo(%Todo{} = todo) do
     Repo.delete(todo)
  end
end
